# Development environment setup

Use these instructions to run the GitLab plugin locally.

Prerequisites:

- You have [created a GitLab account](https://gitlab.com/users/sign_up). It is free and it is awesome!
- You have [IntelliJ IDEA CE](https://www.jetbrains.com/idea/download)
  or [IntelliJ IDEA Ultimate](https://www.jetbrains.com/idea/download) installed.
- You have the [Gradle](https://www.jetbrains.com/help/idea/gradle.html) plugin for JetBrains installed
  and configured.

To do this:

1. [Fork and clone the project](#fork-and-clone-the-project).
1. [Run the plugin](#run-the-plugin).
1. [Run tests](#run-tests).
1. [Add documentation](#add-documentation).

## Fork and clone the project

1. Use your GitLab account to
   [fork this project](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/forks/new).
   Need help? See this [guide to forking a project](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html#doc-nav).
1. Go to your forked project. The URL should be similar to
   `https://gitlab.com/<username>/gitlab-jetbrains-plugin`.
1. On the right-hand side of the page, select **Clone**. Copy the SSH or HTTPS URL to clone the project to your local machine.
   Need help? See this [guide to cloning a project](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository).
1. In IntelliJ IDEA,
   [open your cloned project](https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html#open-project).

## Run the plugin

To run the plugin in IntelliJ IDEA:

1. In IntelliJ IDEA, on the navigation bar, select **Run plugin**.
1. In the dropdown list, under **Run Configuration**, select **Run Plugin**.

## Run tests

To run tests in IntelliJ IDEA:

1. In IntelliJ IDEA, on the navigation bar, select **Run plugin**.
1. In the dropdown list, under **Run Configuration**, select **Run Tests**.

## Add documentation

If you add a new feature or change an existing feature, document it in the README.

To add documentation that includes a new image:

1. Add images into the `docs/assets` folder, and commit the changes.
1. Edit the README file, and insert full permalinks to your new images.
   The permalinks contain the commit SHA from your first commit, and are
   in the form of:

   ```plaintext
   https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/raw/<COMMIT_SHA>/docs/assets/imagename.png
   ```

1. Commit your text changes.

## Using Snowplow Micro

Before following the instructions below, please ensure that Docker is installed and working. This walkthrough has
been tested using [Rancher Desktop](https://rancherdesktop.io/). For other Docker alternatives, please visit
this [handbook page](https://handbook.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop).

If you are confirming any changes to the Snowplow metrics or Iglu schemas, you will likely need to have [Snowplow micro](https://docs.snowplow.io/docs/testing-debugging/snowplow-micro/what-is-micro/)
configured for testing purposes. This can be done in one of two ways:

1. [Configure Snowplow micro using the GitLab-Development-Kit (GDK)](https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/local_setup_and_debugging.html)
1. [Spin up a dockerized version of Snowplow micro](https://gitlab.com/gitlab-org/snowplow-micro-configuration)

Regardless of the way Snowplow micro is configured, there is a simple change that needs to be made locally to redirect
metrics from the GitLab Snowplow collector to your local machine. The changes will take place in `GitLabApplicationService`
in a variable that mentions `SnowPlowDestination`, and more specifically, the `createTracker` method:

```shell
  val snowplowTracker by lazy {
    with(SnowplowDestination) {
            createTracker(COLLECTOR_URL, EMITTER_BATCH_SIZE)
    }
  }
```

In order to redirect Snowplow to send metrics to your local machine, the `COLLECTOR_URL` and `EMITTER_BATCH_SIZE` variables
need to change. Pass `http://localhost:9090` for `COLLECTOR_URL` and 1 for `EMITTER_BATCH_SIZE`:

```shell
  val snowplowTracker by lazy {
    with(SnowplowDestination) {
      -      createTracker(COLLECTOR_URL, EMITTER_BATCH_SIZE)
      +      createTracker("http://localhost:9090", 1)
    }
  }
```

Once this change is made, follow the instructions in whichever way you have decided to spin up Snowplow micro locally.
