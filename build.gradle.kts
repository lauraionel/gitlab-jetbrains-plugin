import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.changelog.Changelog
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// Read properties from gradle.properties to be used in here
fun properties(key: String) = project.findProperty(key).toString()

plugins {
  val kotlinVersion = "1.9.10"

  id("java")
  kotlin("jvm") version kotlinVersion
  kotlin("plugin.serialization") version kotlinVersion
  id("org.jetbrains.intellij") version "1.16.1"
  id("org.jetbrains.changelog") version "2.2.0"
  id("io.gitlab.arturbosch.detekt") version "1.23.4"
  id("com.github.gmazzo.buildconfig") version "5.3.2"
}

group = "com.gitlab.plugin"
version = properties("plugin.version")

repositories {
  mavenCentral()
  maven("https://packages.jetbrains.team/maven/p/ij/intellij-dependencies")
}

buildConfig {
  packageName(group.toString())

  val localBuild = System.getenv("CI").isNullOrBlank()
  buildConfigField("Boolean", "DUO_CHAT_ENABLED", localBuild)
}

detekt {
  buildUponDefaultConfig = true // preconfigure defaults
  allRules = true // activate all available (even unstable) rules.
  config.setFrom("detekt.yml")
}

tasks.withType<Detekt>().configureEach {
  jvmTarget = JavaVersion.VERSION_17.toString()

  reports {
    html.required.set(true) // observe findings in your browser with structure and code snippets
  }
}

configurations.implementation {
  exclude("org.jetbrains.kotlinx", "kotlinx-coroutines-core")
}

val remoteRobotVersion = "0.11.20"
dependencies {
  val ktorVersion = properties("ktor.version")
  val kotlinVersion = properties("kotlin.version")
  val snowplowVersion = properties("snowplow.version")
  val junitVersion = properties("junit.version")

  implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
  implementation("io.ktor:ktor-client-auth:$ktorVersion")
  implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
  implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
  implementation("io.ktor:ktor-serialization-gson:$ktorVersion")
  implementation("io.ktor:ktor-client-logging:$ktorVersion")
  implementation("ch.qos.logback:logback-classic:1.4.14")
  implementation("com.snowplowanalytics:snowplow-java-tracker:$snowplowVersion")
  implementation("com.snowplowanalytics:snowplow-java-tracker:$snowplowVersion") {
    capabilities {
      requireCapability("com.snowplowanalytics:snowplow-java-tracker-okhttp-support")
    }
    constraints {
      api("com.fasterxml.jackson.core:jackson-databind:2.15.3") {
        because("previous version has a security vulnerability")
      }
      api("commons-net:commons-net:3.9.0") {
        because("previous version has a security vulnerability")
      }
    }
  }
  testImplementation("io.ktor:ktor-client-mock:$ktorVersion")
  testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.1")
  testImplementation("org.junit.jupiter:junit-jupiter-params:$junitVersion")
  testImplementation("org.junit.jupiter:junit-jupiter:$junitVersion")
  testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")
  testImplementation("io.mockk:mockk:1.13.5")
  testImplementation("org.mock-server:mockserver-junit-jupiter-no-dependencies:5.14.0")

  // Kotest
  val kotestVersion = properties("kotest.version")
  testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
  testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
  testImplementation("io.kotest:kotest-framework-datatest:$kotestVersion")

  // e2eTest dependencies
  testImplementation("com.intellij.remoterobot:remote-robot:$remoteRobotVersion")
  testImplementation("com.intellij.remoterobot:remote-fixtures:$remoteRobotVersion")
  testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
  testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.9.3")
  testRuntimeOnly("org.junit.platform:junit-platform-suite-engine:1.10.1")

  // Logging Network Calls
  testImplementation("com.squareup.okhttp3:logging-interceptor:4.11.0")

  // Video Recording
  testImplementation("com.automation-remarks:video-recorder-junit5:2.0") {
    exclude(group = "log4j", module = "log4j")
  }

  // Linting
  detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.23.4")
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
  version = properties("platform.version")
  type = properties("platform.type")

  plugins = listOf("Git4Idea")
}

// Configure Gradle Changelog Plugin - read more: https://github.com/JetBrains/gradle-changelog-plugin
changelog {
  version = properties("plugin.version")
  groups = listOf("Added", "Changed", "Removed", "Fixed")

  repositoryUrl =
    System.getenv("CI_PROJECT_URL") ?: "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin"
}

tasks {
  withType<Test>().configureEach {
    useJUnitPlatform()
  }

  // Set the JVM compatibility versions
  withType<JavaCompile> {
    sourceCompatibility = properties("platform.java.version")
    targetCompatibility = properties("platform.java.version")
  }
  withType<KotlinCompile> {
    kotlinOptions {
      jvmTarget = properties("platform.java.version")
    }
  }

  wrapper {
    gradleVersion = properties("gradle.version")
  }

  patchPluginXml {
    sinceBuild = properties("plugin.minBuild")
    untilBuild = properties("plugin.maxBuild")

    changeNotes = provider {
      changelog.renderItem(
        changelog
          .getLatest()
          .withHeader(false)
          .withEmptySections(false),
        Changelog.OutputType.HTML
      )
    }
  }

  signPlugin {
    certificateChain = System.getenv("CERTIFICATE_CHAIN")
    privateKey = System.getenv("PRIVATE_KEY")
    password = System.getenv("PRIVATE_KEY_PASSWORD")
  }

  publishPlugin {
    token = System.getenv("PUBLISH_TOKEN")
  }

  test {
    description = "Runs the test suite. Set project property `-Pe2eTests` to run the E2E tests."
    useJUnitPlatform()

    if (!project.hasProperty("e2eTests")) {
      filter {
        excludeTestsMatching("com.gitlab.plugin.e2eTest.*")
      }
    } else {
      testLogging {
        events("passed", "skipped", "failed", "standardOut", "standardError")
      }
      filter {
        includeTestsMatching("com.gitlab.plugin.e2eTest.*")
      }
    }
  }

  runPluginVerifier {
    ideVersions = listOf("${properties("platform.type")}-${properties("platform.version")}")
    localPaths = emptyList()
  }

  runIdeForUiTests {
    systemProperty("ide.mac.message.dialogs.as.sheets", "false")
    systemProperty("jb.privacy.policy.text", "<!--999.999-->")
    systemProperty("jb.consents.confirmation.enabled", "false")
    systemProperty("ide.mac.file.chooser.native", "false")
    systemProperty("jbScreenMenuBar.enabled", "false")
    systemProperty("apple.laf.useScreenMenuBar", "false")

    systemProperty("robot-server.port", "8082") // default port 8580
    systemProperty("idea.trust.all.projects", "true")
    systemProperty("ide.show.tips.on.startup.default.value", "false")

    // Avoid slow operations assertion https://youtrack.jetbrains.com/issue/IDEA-275489
    systemProperty("ide.slow.operations.assertion", "false")
  }

  downloadRobotServerPlugin {
    version = remoteRobotVersion
  }
}
