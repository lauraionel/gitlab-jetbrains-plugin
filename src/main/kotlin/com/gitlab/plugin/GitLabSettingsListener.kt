package com.gitlab.plugin

import com.intellij.util.messages.Topic

interface GitLabSettingsListener {
  companion object {
    val SETTINGS_CHANGED = Topic.create(
      "com.gitlab.plugin.GitLabSettingsListener",
      GitLabSettingsListener::class.java
    )
  }

  fun codeStyleSettingsChanged(event: GitLabSettingsChangeEvent)
}
