package com.gitlab.plugin.util

import com.gitlab.plugin.api.CodeSuggestionsTokenDto
import com.gitlab.plugin.api.GitLabApi
import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.services.DuoContextService
import kotlinx.coroutines.runBlocking

class JwtSession private constructor() {
  companion object {
    @Volatile
    private var instance: JwtSession? = null

    fun getInstance() =
      instance ?: synchronized(this) {
        instance ?: JwtSession().also { instance = it }
      }
  }

  private var jwtToken: CodeSuggestionsTokenDto? = null

  /**
   * Generate or return an existing JWT token
   *
   * If token is about to expire it retrieves a new one
   *
   * @return String - the JWT token
   */
  @Throws(GitLabUnauthorizedException::class, GitLabOfflineException::class)
  fun getJWT(): String {
    if (jwtToken == null || jwtToken?.isExpired() == true) {
      val duoContext = DuoContextService.instance
      val token = TokenUtil.getToken() ?: ""
      val duoSettings = duoContext.duoSettings
      val url = duoSettings.url
      val api = GitLabApi(token, url)

      if (!api.isConfigured()) {
        return ""
      }

      runBlocking {
        jwtToken = api.generateCodeSuggestionsToken()
      }
    }

    return jwtToken?.accessToken ?: ""
  }

  /**
   * Expire existing JWT token
   *
   * This nullifies the memoization so next time the token is requested is a new one
   */
  fun expireJWT() {
    jwtToken = null
  }

  /**
   * Refresh the JWT token
   *
   * Expires current one and retrieve a new one
   *
   * @return String - a new JWT token
   */
  fun refreshJWT(): String {
    expireJWT()

    return getJWT()
  }
}
