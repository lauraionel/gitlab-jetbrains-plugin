package com.gitlab.plugin.util

import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.credentialStore.generateServiceName
import com.intellij.ide.passwordSafe.PasswordSafe

object TokenUtil {

  fun getToken(): String? {
    val credentialAttributes = createTokenCredentialAttributes()
    return PasswordSafe.instance.getPassword(credentialAttributes)
  }

  fun setToken(token: String) {
    val credentialAttributes = createTokenCredentialAttributes()
    PasswordSafe.instance.set(credentialAttributes, Credentials("", token))

    gitlabStatusRefresh()
  }

  private fun createTokenCredentialAttributes(): CredentialAttributes =
    CredentialAttributes(generateServiceName(GitLabUtil.SERVICE_NAME, "GitLab Personal Access Token"))
}
