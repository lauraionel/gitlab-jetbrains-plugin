package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.JwtProvider
import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.proxiedEngine
import com.gitlab.plugin.codesuggestions.telemetry.LogDestination
import com.gitlab.plugin.codesuggestions.telemetry.SnowplowDestination
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import com.gitlab.plugin.ui.CompletionStrategy
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.intellij.openapi.project.Project

class ProjectContextService(val project: Project) {
  private val patProvider by lazy { PatProvider() }

  private val jwtProvider by lazy { JwtProvider() }

  private val notificationManager by lazy { GitLabNotificationManager() }

  private val exceptionHandler by lazy { CreateNotificationExceptionHandler(notificationManager) }

  private val duoContext: DuoContextService by lazy { DuoContextService.instance }

  val duoHttpClient by lazy {
    DuoClient(
      tokenProvider = patProvider,
      jwtProvider = jwtProvider,
      exceptionHandler = exceptionHandler,
      userAgent = duoContext.userAgent,
      host = duoContext.duoSettings.url,
      onStatusChanged = duoContext.iconStatusModifier,
      httpClientEngine = proxiedEngine()
    )
  }

  val duoApi by lazy { DuoApi(client = duoHttpClient, telemetry = telemetry, duoSettings = duoContext.duoSettings) }

  val serverService by lazy { GitLabServerService(duoApi) }

  val completionStrategy by lazy { CompletionStrategy(duoApi, serverService) }

  val telemetry by lazy {
    val snowplowTracker = GitLabApplicationService.getInstance().snowplowTracker
    Telemetry(
      listOf(LogDestination(), SnowplowDestination(snowplowTracker)),
      isEnabled = { duoContext.duoSettings.telemetryEnabled })
  }

  companion object {
    lateinit var instance: ProjectContextService

    fun init(project: Project): ProjectContextService {
      instance = ProjectContextService(project)
      return instance
    }
  }
}
