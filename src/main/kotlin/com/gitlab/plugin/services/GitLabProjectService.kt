package com.gitlab.plugin.services

import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.dvcs.repo.VcsRepositoryMappingListener
import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.guessProjectDir
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.vfs.VirtualFile
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryManager
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@Service(Service.Level.PROJECT)
class GitLabProjectService(private val project: Project) : Disposable {
  private val disposable = Disposer.newDisposable()

  /**
   * Retrieve the repository for current project and execute the block with it
   *
   * If the repository is already loaded the block is executed immediately,
   * otherwise it waits until it is fully loaded
   */
  fun fetchRepository(): GitRepository? {
    val projectDirectory = project.guessProjectDir() ?: return null
    var repository: GitRepository?

    runBlocking {
      repository = getRepositoryForPath(projectDirectory)

      if (repository == null) {
        waitForVcsRepositoryMappingUpdated()
        repository = getRepositoryForPath(projectDirectory)
      }
    }

    return repository
  }

  /**
   * Retrieve the repository for current project and execute the block with it
   *
   * If the repository is already loaded the block is executed immediately,
   * otherwise it waits for VCS update with a 3 seconds timeout
   */
  fun withRepository(block: (GitRepository?) -> Unit) {
    val projectDirectory = project.guessProjectDir() ?: return
    var repository: GitRepository?

    runBlocking {
      repository = getRepositoryForPath(projectDirectory)

      if (repository == null) {
        waitForVcsRepositoryMappingUpdated()
        repository = getRepositoryForPath(projectDirectory)
      }

      block.invoke(getRepositoryForPath(projectDirectory))
    }
  }

  /**
   * Retrieves the repository for the given directory location
   */
  private suspend fun getRepositoryForPath(projectDirectory: VirtualFile): GitRepository? = coroutineScope {
    val repo = async {
      val gitRepositoryManager = GitRepositoryManager.getInstance(project)
      gitRepositoryManager.getRepositoryForFile(projectDirectory)
    }
    repo.await()
  }

  private fun waitForVcsRepositoryMappingUpdated(): Boolean {
    val updateFinishedFuture = CompletableFuture<Boolean>()
    val busConnection = project.messageBus.connect()
    busConnection.subscribe(
      VcsRepositoryManager.VCS_REPOSITORY_MAPPING_UPDATED,
      VcsRepositoryMappingListener {
        busConnection.disconnect()

        updateFinishedFuture.complete(true)
      }
    )

    try {
      return updateFinishedFuture.get(3, TimeUnit.SECONDS)
    } catch (ex: TimeoutException) {
      // LOG.warn("Repository mappings update didn't happen", ex)
    } finally {
      busConnection.disconnect()
    }

    return false
  }

  /**
   * Allow object to be disposed
   *
   * For more information see [Disposer and Disposable](https://plugins.jetbrains.com/docs/intellij/disposers.html)
   */
  override fun dispose() {
    disposable.dispose()
  }
}
