package com.gitlab.plugin.services.chat

import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.ui.jcef.JBCefBrowser

@Service(Service.Level.PROJECT)
class ChatBrowserService : Disposable {
  private val browser = JBCefBrowser.createBuilder()
    .setUrl("https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin")
    .build()

  val browserComponent
    get() = browser.component

  override fun dispose() {
    browser.dispose()
  }
}
