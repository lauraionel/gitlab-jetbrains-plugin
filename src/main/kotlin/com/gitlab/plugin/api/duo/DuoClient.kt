package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.*
import com.google.gson.FieldNamingPolicy
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.gson.*
import io.ktor.utils.io.errors.*
import java.net.URI
import java.nio.channels.UnresolvedAddressException

class DuoClient(
  private val tokenProvider: TokenProvider,
  private val jwtProvider: TokenProvider,
  private val exceptionHandler: ExceptionHandler,
  private val userAgent: String,
  private val shouldRetry: Boolean = true,
  private val httpClientEngine: HttpClientEngine = OkHttp.create(),
  private val host: String,
  private val onStatusChanged: DuoClientRequestListener
) {
  fun interface TokenProvider {
    fun token(): String
  }

  interface DuoClientRequestListener {
    fun onRequestStart() {}
    fun onRequestSuccess() {}
    fun onRequestError() {}
  }

  companion object {
    const val MODEL_GATEWAY_ENDPOINT = "https://codesuggestions.gitlab.com/v2/completions"
    private const val HTTP_TIMEOUT = 60_000L
  }

  val restClient = HttpClient(httpClientEngine) {
    expectSuccess = true

    defaultRequest {
      contentType(ContentType.Application.Json)
    }

    install(GitlabStatusPlugin) {
      statusCallback = onStatusChanged
    }

    install(UserAgent) {
      agent = userAgent
    }

    if (shouldRetry) {
      install(HttpRequestRetry) {
        retryOnServerErrors(maxRetries = 3)
        exponentialDelay()
      }
    }

    install(ContentNegotiation) {
      gson {
        setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      }
    }

    install(HttpTimeout) {
      requestTimeoutMillis = HTTP_TIMEOUT
      socketTimeoutMillis = HTTP_TIMEOUT
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { cause: Throwable, request: HttpRequest ->
        onStatusChanged.onRequestError()

        when (cause) {
          is ClientRequestException -> {
            val exceptionResponse = cause.response
            val exceptionResponseText = exceptionResponse.bodyAsText()
            when (exceptionResponse.status) {
              HttpStatusCode.Unauthorized -> throw GitLabUnauthorizedException(exceptionResponse, exceptionResponseText)
              HttpStatusCode.Forbidden -> throw GitLabForbiddenException(exceptionResponse, exceptionResponseText)
              HttpStatusCode.ProxyAuthenticationRequired -> throw GitLabProxyException()
              HttpStatusCode.PayloadTooLarge -> throw GitLabProxyException()
              else -> throw GitLabResponseException(exceptionResponse, exceptionResponseText)
            }
          }

          is UnresolvedAddressException -> throw GitLabOfflineException(request, cause)
          is ConnectTimeoutException -> throw GitLabOfflineException(request, cause)
          is HttpRequestTimeoutException -> throw GitLabOfflineException(request, cause)
          is IOException -> if (cause.message?.contains("431") == true) {
            throw GitLabProxyException()
          } else {
            throw cause
          }

          else -> throw cause
        }
      }
    }
  }

  suspend fun get(endpoint: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? = request(endpoint) {
    method = HttpMethod.Get
    usingToken(tokenProvider)
    block.invoke(this)
  }

  suspend fun post(endpoint: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? = request(endpoint) {
    method = HttpMethod.Post
    usingToken(tokenProvider)
    block.invoke(this)
  }

  @Deprecated("Endpoint deprecated on GitLab server versions < 16.3")
  suspend fun modelGatewayPost(block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? =
    request(MODEL_GATEWAY_ENDPOINT) {
      method = HttpMethod.Post
      usingToken(jwtProvider)
      header("X-Gitlab-Authentication-Type", "oidc")
      block.invoke(this)
    }

  private suspend fun request(endpoint: String, block: HttpRequestBuilder.() -> Unit = {}): HttpResponse? {
    try {
      return restClient.request(URI(host).resolve(endpoint).toString(), block)
    } catch (ex: Exception) {
      exceptionHandler.handleException(ex)
    }

    return null
  }

  fun isConfigured(): Boolean = tokenProvider.token().isNotEmpty()

  fun copy(
    tokenProvider: TokenProvider? = null,
    jwtProvider: TokenProvider? = null,
    exceptionHandler: ExceptionHandler? = null,
    userAgent: String? = null,
    shouldRetry: Boolean? = null,
    host: String? = null,
    httpClientEngine: HttpClientEngine? = null,
    onStatusChanged: DuoClientRequestListener? = null,
  ): DuoClient = DuoClient(
    tokenProvider ?: this.tokenProvider,
    jwtProvider ?: this.jwtProvider,
    exceptionHandler ?: this.exceptionHandler,
    userAgent ?: this.userAgent,
    shouldRetry ?: this.shouldRetry,
    httpClientEngine ?: this.httpClientEngine,
    host ?: this.host,
    onStatusChanged ?: this.onStatusChanged
  )
}

fun HttpRequestBuilder.usingToken(tokenProvider: DuoClient.TokenProvider) {
  headers {
    remove(HttpHeaders.Authorization)
    header(HttpHeaders.Authorization, "Bearer ${tokenProvider.token()}")
  }
}
