package com.gitlab.plugin.activities

import com.gitlab.plugin.services.GitLabProjectService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.util.TokenUtil
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity

class GitLabProjectStartup : ProjectActivity {
  override suspend fun execute(project: Project) {
    val projectService = GitLabProjectService(project)

    val contextService = ProjectContextService.init(project)

    projectService.withRepository { repository ->
      if (repository != null) {
        val token = TokenUtil.getToken()
        val notification = if (!token.isNullOrEmpty()) {
          Notification(
            "Get started with GitLab Duo Code Suggestions.",
            "GitLab Duo Code Suggestions is ready to use with this project."
          )
        } else {
          Notification(
            "Get started with GitLab Duo Code Suggestions.",
            "You need to configure your GitLab credentials first.",
            listOf(NotificationAction.settings(project))
          )
        }

        GitLabNotificationManager().sendNotification(notification)
      }
    }
  }
}
