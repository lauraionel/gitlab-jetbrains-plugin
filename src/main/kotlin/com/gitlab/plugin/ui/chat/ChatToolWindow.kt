package com.gitlab.plugin.ui.chat

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.services.chat.ChatBrowserService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory

internal class ChatToolWindow : ToolWindowFactory {
  override suspend fun isApplicableAsync(project: Project): Boolean = BuildConfig.DUO_CHAT_ENABLED

  override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
    val factory = toolWindow.contentManager.factory
    val chatBrowserService = project.service<ChatBrowserService>()

    val content =
      factory
        .createContent(chatBrowserService.browserComponent, null, false)
        .apply { setDisposer(chatBrowserService) }

    toolWindow.contentManager.addContent(content)
  }
}
