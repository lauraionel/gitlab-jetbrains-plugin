package com.gitlab.plugin.ui

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.services.GitLabServerService

class CompletionStrategy(
  private val duoApi: DuoApi,
  private val serverService: GitLabServerService
) {
  suspend fun generateCompletions(payload: Completion.Payload): Completion.Response? =
    if (serverService.get().after162()) {
      duoApi.completions(payload)
    } else {
      duoApi.completionsLegacy(payload)
    }
}
