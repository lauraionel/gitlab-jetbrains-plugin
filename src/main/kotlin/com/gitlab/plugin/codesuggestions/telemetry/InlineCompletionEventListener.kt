package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.services.ProjectContextService
import com.intellij.codeInsight.inline.completion.InlineCompletionEventAdapter
import com.intellij.codeInsight.inline.completion.InlineCompletionEventType
import com.intellij.codeInsight.inline.completion.logs.InlineCompletionUsageTracker
import org.jetbrains.annotations.ApiStatus

@ApiStatus.Experimental
class InlineCompletionEventListener : InlineCompletionEventAdapter {
  private val telemetry: Telemetry by lazy { ProjectContextService.instance.telemetry }

  override fun onShow(event: InlineCompletionEventType.Show) =
    telemetry.shown(telemetry.currentContext)

  override fun onHide(event: InlineCompletionEventType.Hide) {
    if (!event.isCurrentlyDisplaying || event.finishType == InlineCompletionUsageTracker.ShownEvents.FinishType.SELECTED) return

    telemetry.rejected(telemetry.currentContext)
  }

  override fun onInsert(event: InlineCompletionEventType.Insert) =
    telemetry.accepted(telemetry.currentContext)
}
