package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Metadata
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.lang.module.ModuleDescriptor.Version
import kotlin.test.assertEquals

class GitLabServerServiceTest {

  private val duoApi: DuoApi = mockk<DuoApi>()
  val gitLabServerService = GitLabServerService(duoApi)

  @BeforeEach
  fun setUp() {
    clearMocks(duoApi)
    coEvery { duoApi.metadata() } returns Metadata.Response(version = "1.0")
  }


  @Nested
  inner class Get {
    @Test
    fun `When server data is stale, fetches new data`() {
      val metadata = gitLabServerService.get()

      coVerify(exactly = 1) { duoApi.metadata() }
      assertEquals(Version.parse("1.0"), metadata.version)
    }

    @Test
    fun `When server data is fresh, returns existing data`() {
      val metadata = gitLabServerService.get()

      coEvery { duoApi.metadata() } returns Metadata.Response(version = "1.1")

      gitLabServerService.get()

      coVerify(exactly = 1) { duoApi.metadata() }
      assertEquals(Version.parse("1.0"), metadata.version)
    }
  }
}
