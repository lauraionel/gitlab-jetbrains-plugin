package com.gitlab.plugin.e2eTest.pages

import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.data.RemoteComponent
import com.intellij.remoterobot.fixtures.CommonContainerFixture
import com.intellij.remoterobot.fixtures.DefaultXpath
import com.intellij.remoterobot.fixtures.FixtureName
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.waitForIgnoringError
import java.time.Duration

fun RemoteRobot.duoSettings(function: DuoSettingsFrame.() -> Unit) {
  find<DuoSettingsFrame>(timeout = Duration.ofSeconds(120)).apply(function)
}

private const val GITLAB_INSTANCE_LABEL_TEXT = "URL to GitLab instance"
private const val ACCESS_TOKEN_LABEL_TEXT = "GitLab Personal Access Token"
private const val ENABLE_TELEMETRY_LABEL_TEXT = "Enable telemetry"

@FixtureName("Duo Settings Frame")
@DefaultXpath("type", "//div[@class='MyDialog']")
class DuoSettingsFrame(remoteRobot: RemoteRobot, remoteComponent: RemoteComponent) :
  CommonContainerFixture(remoteRobot, remoteComponent) {

  fun authenticate(instance: String, token: String) {
    step("Authenticate plugin") {
      print("Authenticating plugin against $instance")

      // Wait until GitLab Duo settings are displayed
      waitForIgnoringError { jLabel(ACCESS_TOKEN_LABEL_TEXT).isVisible() }

      textField(GITLAB_INSTANCE_LABEL_TEXT).text = instance
      textField(ACCESS_TOKEN_LABEL_TEXT).text = token

      // disable telemetry in E2E tests
      disableTelemetry()

      // We must click Apply to ensure we use the correct GitLab instance
      // https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/133
      textField(GITLAB_INSTANCE_LABEL_TEXT).click()
      button("Apply").clickWhenEnabled()

      button("Verify setup").clickWhenEnabled()
    }
  }

  private fun disableTelemetry() {
    if (checkBox(ENABLE_TELEMETRY_LABEL_TEXT).isSelected()) checkBox(ENABLE_TELEMETRY_LABEL_TEXT).click()
  }
}
