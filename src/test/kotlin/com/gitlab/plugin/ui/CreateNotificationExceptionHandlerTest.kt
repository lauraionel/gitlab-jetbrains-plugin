package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.GitLabForbiddenException
import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabResponseException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.test.assertFailsWith

class CreateNotificationExceptionHandlerTest {
  private val settingsActionLink = NotificationAction.settings(null)
  private val notificationManagerMock = mockk<GitLabNotificationManager>()
  private val exceptionHandler = CreateNotificationExceptionHandler(notificationManagerMock, settingsActionLink)
  private val unauthorizedNotification = Notification(
    "GitLab Duo",
    GitLabBundle.message("notification.exception.unauthorized.message"),
    listOf(settingsActionLink)
  )
  private val offlineNotification = Notification(
    "GitLab Duo",
    GitLabBundle.message("notification.exception.offline.message")
  )
  private val forbiddenNotification = Notification(
    "GitLab Duo",
    GitLabBundle.message("notification.exception.insufficient-scope.message"),
    listOf(settingsActionLink)
  )
  private val responseNotification = Notification("GitLab Duo", "Request failed with error: An error")
  private val unauthorizedException = mockk<GitLabUnauthorizedException>()
  private val offlineException = mockk<GitLabOfflineException>()
  private val forbiddenException = mockk<GitLabForbiddenException>()
  private val responseException = mockk<GitLabResponseException>()

  @BeforeEach
  fun beforeEach() {
    every { notificationManagerMock.sendNotification(any()) } returns Unit
    every { responseException.cachedResponseText } returns "An error"
  }

  @Nested
  inner class HandleException {
    @Test
    fun `GitlabOfflineException creates notification with unauthorized message only once`() {
      exceptionHandler.handleException(offlineException)
      exceptionHandler.handleException(offlineException)

      verify(exactly = 1) { notificationManagerMock.sendNotification(offlineNotification) }
    }

    @Test
    fun `GitLabUnauthorizedException creates notification with unauthorized message only once`() {
      exceptionHandler.handleException(unauthorizedException)
      exceptionHandler.handleException(unauthorizedException)

      verify(exactly = 1) { notificationManagerMock.sendNotification(unauthorizedNotification) }
    }

    @Test
    fun `GitLabForbiddenException creates notification with unauthorized message only once`() {
      exceptionHandler.handleException(forbiddenException)
      exceptionHandler.handleException(forbiddenException)

      verify(exactly = 1) { notificationManagerMock.sendNotification(forbiddenNotification) }
    }

    @Test
    fun `GitLabResponseException creates notification with unauthorized message only once`() {
      exceptionHandler.handleException(responseException)
      exceptionHandler.handleException(responseException)

      verify(exactly = 1) { notificationManagerMock.sendNotification(responseNotification) }
    }

    @Test
    fun `Other exceptions do not trigger notifications`() {
      try {
        exceptionHandler.handleException(Exception())
      } catch (ex: Exception) {
        verify(exactly = 0) { notificationManagerMock.sendNotification(any()) }
      }
    }

    @Test
    fun `throws non-handled exceptions`() {
      assertFailsWith<RuntimeException> { exceptionHandler.handleException(RuntimeException()) }
    }
  }

  @Nested
  inner class ResetNotifications {
    @Test
    fun `unauthorized notifications can be send again after reset`() {
      exceptionHandler.handleException(unauthorizedException)

      exceptionHandler.resetNotifications()

      exceptionHandler.handleException(unauthorizedException)

      verify(exactly = 2) { notificationManagerMock.sendNotification(unauthorizedNotification) }
    }

    @Test
    fun `offline notifications can be send again after reset`() {
      exceptionHandler.handleException(offlineException)

      exceptionHandler.resetNotifications()

      exceptionHandler.handleException(offlineException)

      verify(exactly = 2) { notificationManagerMock.sendNotification(offlineNotification) }
    }

    @Test
    fun `forbidden notifications can be send again after reset`() {
      exceptionHandler.handleException(forbiddenException)

      exceptionHandler.resetNotifications()

      exceptionHandler.handleException(forbiddenException)

      verify(exactly = 2) { notificationManagerMock.sendNotification(forbiddenNotification) }
    }

    @Test
    fun `response notifications can be send again after reset`() {
      exceptionHandler.handleException(responseException)

      exceptionHandler.resetNotifications()

      exceptionHandler.handleException(responseException)

      verify(exactly = 2) { notificationManagerMock.sendNotification(responseNotification) }
    }
  }
}
