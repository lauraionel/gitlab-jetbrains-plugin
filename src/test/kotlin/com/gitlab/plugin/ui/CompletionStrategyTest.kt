package com.gitlab.plugin.ui

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.services.GitLabServerService
import io.mockk.*
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.lang.module.ModuleDescriptor

class CompletionStrategyTest {
  val duoApi = mockk<DuoApi>()
  val serverService = mockk<GitLabServerService>()

  @BeforeEach
  fun setUp() {
    clearMocks(duoApi, serverService)

    coEvery { duoApi.completions(any()) } returns null
    coEvery { duoApi.completionsLegacy(any()) } returns null
  }

  @Nested
  inner class GenerateCompletions {
    @Test
    fun `when before 16_3 uses legacy completion`() = runTest {
      every { serverService.get() } returns GitLabServerService.Server(ModuleDescriptor.Version.parse("16.2"), 0)

      val payload =
        Completion.Payload(
          currentFile = Completion.Payload.CodeSuggestionRequestFile(contentAboveCursor = "abcdefghijklmnop")
        )

      CompletionStrategy(duoApi, serverService).generateCompletions(payload)

      coVerify(exactly = 1) { duoApi.completionsLegacy(any()) }
      coVerify(exactly = 0) { duoApi.completions(any()) }
    }

    @Test
    fun `when 16_3 or later uses new completion`() = runTest {
      every { serverService.get() } returns GitLabServerService.Server(ModuleDescriptor.Version.parse("16.3"), 0)

      val payload =
        Completion.Payload(
          currentFile = Completion.Payload.CodeSuggestionRequestFile(contentAboveCursor = "abcdefghijklmnop")
        )

      CompletionStrategy(duoApi, serverService).generateCompletions(payload)

      coVerify(exactly = 0) { duoApi.completionsLegacy(any()) }
      coVerify(exactly = 1) { duoApi.completions(any()) }
    }

    @Test
    fun `when 16_2_0-ee uses legacy completion`() = runTest {
      every { serverService.get() } returns GitLabServerService.Server(ModuleDescriptor.Version.parse("16.2.0-ee"), 0)

      val payload =
        Completion.Payload(
          currentFile = Completion.Payload.CodeSuggestionRequestFile(contentAboveCursor = "abcdefghijklmnop")
        )

      CompletionStrategy(duoApi, serverService).generateCompletions(payload)

      coVerify(exactly = 1) { duoApi.completionsLegacy(any()) }
      coVerify(exactly = 0) { duoApi.completions(any()) }
    }

    @Test
    fun `when 16_3_0-ee uses new completion`() = runTest {
      every { serverService.get() } returns GitLabServerService.Server(ModuleDescriptor.Version.parse("16.3.0-ee"), 0)

      val payload =
        Completion.Payload(
          currentFile = Completion.Payload.CodeSuggestionRequestFile(contentAboveCursor = "abcdefghijklmnop")
        )

      CompletionStrategy(duoApi, serverService).generateCompletions(payload)

      coVerify(exactly = 0) { duoApi.completionsLegacy(any()) }
      coVerify(exactly = 1) { duoApi.completions(any()) }
    }
  }
}
