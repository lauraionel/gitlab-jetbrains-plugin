# What does this MR do and why?

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

/label ~"Editor Extensions::JetBrains" ~"Category:Editor Extensions" ~"devops::create" ~"group::editor extensions" ~"section::dev"

/assign me

# MR acceptance checklist

- [ ] If this is a user-facing change, add an entry to `CHANGELOG.md` 
- [ ] Manually test and verify the changes
